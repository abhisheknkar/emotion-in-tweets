import json, csv
import os
from collections import OrderedDict
import pandas as pd
from scipy.io.arff import loadarff
# import progressbar
# from spacy.tokenizer import Tokenizer
import numpy as np

class LexiconFeatures():
    def __init__(self, lexicondir):
        self.lexicondir = lexicondir

    def build(self, path):
        df = pd.DataFrame({'word':[]})
        df = pd.merge(df, self.getBingLiu(), on='word', how='outer')
        df = pd.merge(df, self.getMPQA(), on='word', how='outer')
        df = pd.merge(df, self.getAFINNWords(), on='word', how='outer')
        df = pd.merge(df, self.getAFINNEmoticons(), on='word', how='outer')
        df = pd.merge(df, self.getS140(), on='word', how='outer')
        df = pd.merge(df, self.getNRCEmotions(), on='word', how='outer')
        df = pd.merge(df, self.getNRCHashSent(), on='word', how='outer')
        df = pd.merge(df, self.getNRCHashEmotions(), on='word', how='outer')
        df = pd.merge(df, self.getNegations(), on='word', how='outer')
        df = pd.merge(df, self.getNRCEmotionsExpanded(), on='word', how='outer')
        df['word'] = df['word'].drop_duplicates()

        df.fillna(0.0, inplace=True)
        df.to_csv(path, sep='\t', index=False)
        self.df = df
        self.df.set_index(['word'], inplace=True)
        self.index = {}
        count = 0
        for word in list(self.df.index):
            self.index[word] = count
            count += 1
        self.ftrmat = self.df[self.df.columns].values

    def load(self, path, sep='\t'):
        self.df = pd.read_csv(path, sep=sep)
        # return self.df

    def getBingLiu(self):
        df = pd.read_csv(os.path.join(self.lexicondir, 'BingLiu.csv.gz'), sep='\t')
        df.columns = ['word', 'lex-bingliu']
        df[df.columns[1] + '-pos'] = (df[df.columns[1]] == 'positive').apply(int)
        df[df.columns[1] + '-neg'] = (df[df.columns[1]] == 'negative').apply(int)
        df.drop(df.columns[1], 1, inplace=True)
        return df

    def getMPQA(self):
        df = pd.read_csv(os.path.join(self.lexicondir, 'mpqa.txt.gz'), sep='\t')
        df.columns = ['word', 'lex-mpqa']
        df[df.columns[1] + '-pos'] = (df[df.columns[1]] == 'positive').apply(int)
        df[df.columns[1] + '-neg'] = (df[df.columns[1]] == 'negative').apply(int)
        df.drop(df.columns[1], 1, inplace=True)
        return df

    def getAFINNWords(self):
        df = pd.read_csv(os.path.join(self.lexicondir, 'AFINN-en-165.txt.gz'), sep='\t')
        df.columns = ['word', 'lex-AFINN-word']
        df[df.columns[1] + '-pos'] = df[df.columns[1]].apply(lambda x: x if x > 0 else 0)
        df[df.columns[1] + '-neg'] = df[df.columns[1]].apply(lambda x: x if x < 0 else 0)
        df.drop(df.columns[1], 1, inplace=True)
        return df

    def getAFINNEmoticons(self):
        df = pd.read_csv(os.path.join(self.lexicondir, 'AFINN-emoticon-8.txt.gz'), sep='\t')
        df.columns = ['word', 'lex-AFINN-emoticon']
        df[df.columns[1] + '-pos'] = df[df.columns[1]].apply(lambda x: x if x > 0 else 0)
        df[df.columns[1] + '-neg'] = df[df.columns[1]].apply(lambda x: x if x < 0 else 0)
        df.drop(df.columns[1], 1, inplace=True)
        return df

    def getS140(self):
        df = pd.read_csv(os.path.join(self.lexicondir, 'Sentiment140-Lexicon-v0.1/unigrams-pmilexicon.txt.gz'), sep='\t')
        df.columns = ['word', 'lex-S140', 'poscount', 'negcount']
        df[df.columns[1] + '-pos'] = df[df.columns[1]].apply(lambda x: x if x > 0 else 0)
        df[df.columns[1] + '-neg'] = df[df.columns[1]].apply(lambda x: x if x < 0 else 0)
        df.drop(['lex-S140', 'poscount', 'negcount'], 1, inplace=True)
        return df

    def getNRCEmotions(self):
        df = pd.read_csv(os.path.join(self.lexicondir, 'NRC-emotion-lexicon-wordlevel-v0.92.txt.gz'), sep='\t')
        df.columns = ['word', 'emotion', 'lex-NRC10']
        df = pd.pivot_table(df, values='lex-NRC10', index=['word'], columns=['emotion'])
        df.reset_index(level=0, inplace=True)
        newCols = ['word'] + ['lex-NRC10-' + x for x in df.columns[1:]]
        df.columns = newCols
        return df

    def getNRCHashSent(self):
        df = pd.read_csv(os.path.join(self.lexicondir, 'NRC-Hashtag-Sentiment-Lexicon-v0.1/unigrams-pmilexicon.txt.gz'), sep='\t')
        df.columns = ['word', 'lex-NRC-HashSent', 'poscount', 'negcount']

        # df['word'] = df['word'].apply(lambda x: x[1:] if str(x)[0] == '#' else x)
        # df['word'] = df['word'].drop_duplicates()

        df[df.columns[1] + '-pos'] = df[df.columns[1]].apply(lambda x: x if x > 0 else 0)
        df[df.columns[1] + '-neg'] = df[df.columns[1]].apply(lambda x: x if x < 0 else 0)
        df.drop(['lex-NRC-HashSent', 'poscount', 'negcount'], 1, inplace=True)
        return df

    def getNRCHashEmotions(self):
        df = pd.read_csv(os.path.join(self.lexicondir, 'NRC-Hashtag-Emotion-Lexicon-v0.2.txt.gz'), sep='\t', skiprows=34, header=None)
        df.columns = ['emotion', 'word', 'lex-NRC-Hash-Emo']

        # df['word'] = df['word'].apply(lambda x: x[1:] if str(x)[0] == '#' else x)
        # df['word'] = df['word'].drop_duplicates()

        df = pd.pivot_table(df, values='lex-NRC-Hash-Emo', index=['word'], columns=['emotion'])
        df.reset_index(level=0, inplace=True)
        newCols = ['word'] + ['lex-NRC-Hash-Emo-' + x for x in df.columns[1:]]
        df.columns = newCols
        return df

    def getNegations(self):
        df = pd.read_csv(os.path.join(self.lexicondir, 'NegatingWordList.txt.gz'), sep='\t', header=None)
        df.columns = ['word']
        df = df.append(pd.DataFrame({'word': ['n\'t']}), ignore_index=True)
        df['lex-negation'] = 1
        return df

    def getNRCEmotionsExpanded(self):
        # Downloaded from the AffectiveTweets site: https://www.cs.waikato.ac.nz/ml/sa/lex.html#emolextwitter
        df = pd.read_csv(os.path.join(self.lexicondir, 'NRC-expanded.csv'), sep='\t')
        df.columns = ['word'] + ['lex-NRC10-Expanded-' + x for x in df.columns[1:]]
        return df

        # arff_path = os.path.join(self.lexicondir, 'arff_lexicons/NRC-AffectIntensity-Lexicon.arff')
        # csv_path = arff_path[:-4] + 'csv'
        # if not os.path.exists(csv_path):
        #     print('Converting NRC-Expanded to CSV...')
        #     arff2csv(arff_path, csv_path, javacommand=wekaCommand, separator='\t')
        #     print('Done!')
        # df = pd.read_csv(csv_path, sep='\t')
        # df.replace('?', 0.0, inplace=True)
        # newCols = ['word'] + ['lex-NRC-Expanded-' + x[:-6] for x in df.columns[1:]]
        # df.columns = newCols
        # return df

    def vectorizeDoc(self, tweet):
        doc = tweet.split()
        # tokens = [token.text for token in doc]
        # tokens = [token.lemma_ for token in doc]
        inLexicon = [self.index[word] for word in doc if word in self.index]
        df2 = self.ftrmat[inLexicon,:]
        df2 = np.sum(df2, axis=0)
        # df2 = df2.sum()
        # df2['tweet'] = tweet
        return df2

    def vectorizeDataset(self, tweets, updateInterval=100):
        N = len(tweets)
        ftrs = np.zeros((N, self.ftrmat.shape[1]))

        for idx,tweet in enumerate(tweets):
            ftrs[idx,:] = self.vectorizeDoc(tweet)
        df = pd.DataFrame()
        df['tweet'] = tweets
        for i in range(ftrs.shape[1]):
            df[self.df.columns[i+1]] = ftrs[:,i]
        return df


if __name__ == '__main__':
    # To extract features for the SemEval 2018 dataset
    weka_version = 'weka-3-8-1'
    wekaCommand = 'java -Xmx20G -cp $HOME/tools/' + weka_version + '/weka.jar weka.Run'
    lexiconspath = '../models/lexicons.csv'

    a = FeatureGenerator('configs/config.json', 'configs/globaloptions.json')
    a.runfiltersLex()


    # To extract features for general inputs
    # a = LexiconFeatures(lexicondir='/home/abhishek/wekafiles/packages/AffectiveTweets/lexicons/')
    # lexiconsPath = '../outputs/intermediate/temp.csv'
    # a.build(path=lexiconsPath)
    # a.load(path=lexiconsPath)

    # glovepath = '/home/abhishek/Downloads/glove.6B/glove.6B.50d.txt'
    # b = GloveFeatures(glovepath)
    # inputs = ["good",
    #           "good good",
    #           "bad",
    #           "bad bad",
    #           ]
    # X_pandas = b.vectorizeDataset(inputs)
    # print(X_pandas.transpose())
