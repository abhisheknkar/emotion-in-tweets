
# coding: utf-8

# <h1>Table of Contents<span class="tocSkip"></span></h1>
# <div class="toc"><ul class="toc-item"><li><span><a href="#Getting-Data-Ready" data-toc-modified-id="Getting-Data-Ready-1"><span class="toc-item-num">1&nbsp;&nbsp;</span>Getting Data Ready</a></span><ul class="toc-item"><li><span><a href="#Preprocessing" data-toc-modified-id="Preprocessing-1.1"><span class="toc-item-num">1.1&nbsp;&nbsp;</span>Preprocessing</a></span></li><li><span><a href="#Loading-Data" data-toc-modified-id="Loading-Data-1.2"><span class="toc-item-num">1.2&nbsp;&nbsp;</span>Loading Data</a></span></li><li><span><a href="#Tokenizing-Data" data-toc-modified-id="Tokenizing-Data-1.3"><span class="toc-item-num">1.3&nbsp;&nbsp;</span>Tokenizing Data</a></span></li><li><span><a href="#Loading-Word-Embeddings" data-toc-modified-id="Loading-Word-Embeddings-1.4"><span class="toc-item-num">1.4&nbsp;&nbsp;</span>Loading Word Embeddings</a></span></li></ul></li><li><span><a href="#Baseline-and-Traditional-Models" data-toc-modified-id="Baseline-and-Traditional-Models-2"><span class="toc-item-num">2&nbsp;&nbsp;</span>Baseline and Traditional Models</a></span></li><li><span><a href="#Deep-Neural-Network" data-toc-modified-id="Deep-Neural-Network-3"><span class="toc-item-num">3&nbsp;&nbsp;</span>Deep Neural Network</a></span><ul class="toc-item"><li><span><a href="#Architecture-Creation" data-toc-modified-id="Architecture-Creation-3.1"><span class="toc-item-num">3.1&nbsp;&nbsp;</span>Architecture Creation</a></span></li><li><span><a href="#Training-and-Evaluation" data-toc-modified-id="Training-and-Evaluation-3.2"><span class="toc-item-num">3.2&nbsp;&nbsp;</span>Training and Evaluation</a></span></li><li><span><a href="#Testing" data-toc-modified-id="Testing-3.3"><span class="toc-item-num">3.3&nbsp;&nbsp;</span>Testing</a></span></li></ul></li><li><span><a href="#Notes-(in-progress)" data-toc-modified-id="Notes-(in-progress)-4"><span class="toc-item-num">4&nbsp;&nbsp;</span>Notes (in progress)</a></span><ul class="toc-item"><li><span><a href="#Preprocessing" data-toc-modified-id="Preprocessing-4.1"><span class="toc-item-num">4.1&nbsp;&nbsp;</span>Preprocessing</a></span></li><li><span><a href="#Data" data-toc-modified-id="Data-4.2"><span class="toc-item-num">4.2&nbsp;&nbsp;</span>Data</a></span></li><li><span><a href="#Model-Selection" data-toc-modified-id="Model-Selection-4.3"><span class="toc-item-num">4.3&nbsp;&nbsp;</span>Model Selection</a></span></li><li><span><a href="#Training" data-toc-modified-id="Training-4.4"><span class="toc-item-num">4.4&nbsp;&nbsp;</span>Training</a></span></li></ul></li></ul></div>

# First, load the relevant packages: ``pip install numpy pandas regex emoji sklearn scipy keras tensorflow``

# In[24]:


# from IPython.core.display import display, HTML
# display(HTML("<style>.container { width:100% !important; }</style>")) # To make the cells wider


# In[2]:


import numpy as np
import pandas as pd
import os, sys


# # Getting Data Ready

# ## Preprocessing
# Preprocessing is critical for noisy texts such as tweets. Consider the following example: 
# 
# ``@bri243 This was soooo cool :) :) The BEST 2 days ever!!! 💙💙💙 😊 #Epic #RoadTrip #Spain2014``
# 
# There are several elements in this tweet that the model we train may not understand. For instance, it contains:
# * ``@bri245``: a user mention
# * ``soooo``, ``!!!``: text elongation, punctuation repetition
# * ``:)``: a smiley in plain ASCII text
# * ``BEST``: text in all-caps
# * ``💙``: emoji in unicode
# * ``Epic``: a unigram hashtag
# * ``RoadTrip``: a bigram hashtag
# * ``2``: numerals
# * ``\n``: a newline character
# 
# Using the preprocessing below, we will convert transform this input to the following:
# 
# ``<user> this was so <elong> cool <smile> <smile> <elong> the best <allcaps> <number> days ever ! <repeat> <emoji> blue heart <emoji> blue heart <emoji> blue heart <emoji> smiling face with smiling eyes  <hashtag> epic <hashtag> road trip <hashtag> spain<number>'``
# 
# Note how we reduced elements like user mentions, text elongations, etc. to appropriate tags. The tags allow the model to understand the meaning of all these elements. Without preprocessing, either the model would have ignored the noisy text, or would have been misled by it.
# 
# To perform the preprocessing, we use the [preprocessing script](https://nlp.stanford.edu/projects/glove/preprocess-twitter.rb) from the [official GloVe page](https://nlp.stanford.edu/projects/glove/), translated to Python, thanks to [@tokestermw](https://gist.github.com/tokestermw/cb87a97113da12acb388). I've fixed a few bugs in that script to create this. Also added script to replace escape characters (\n, \t, &amp). Here'e the script.

# In[3]:


import sys
import regex as re
import emoji

FLAGS = re.MULTILINE | re.DOTALL # | re.UNICODE
emoji_dict = emoji.UNICODE_EMOJI

def hashtag(text):
    text = text.group()
    hashtag_body = text[1:]
    if hashtag_body.isupper():
#         result = " {} ".format(hashtag_body.lower())
        result = " ".join(["<hashtag>"] + [hashtag_body.lower()] + ["<allcaps>"])
    else:
        result = " ".join(["<hashtag>"] + re.sub(r'([A-Z])', r' \1', hashtag_body, flags=FLAGS).split())
    return result

def allcaps(text):
    text = text.group()
    return text.lower() + " <allcaps>"

def demojize2(text):
    return ''.join([c if c not in emoji_dict else ' ' + ' '.join(['<emoji>'] + emoji_dict[c][1:-1].split('_')) + ' ' for c in text])

def preprocess(text):
    # Different regex parts for smiley faces
    eyes = r"[8:=;]"
    nose = r"['`\-]?"

    # function so code less repetitive
    def re_sub(pattern, repl):
        return re.sub(pattern, repl, text, flags=FLAGS)

    text = re_sub(r"(\\n|\\t|\n|\t|\r|&amp)", " ")    
   
    text = re_sub(r"https?:\/\/\S+\b|www\.(\w+\.)+\S*", " <url> ")
    text = re_sub(r"@\w+", " <user> ")
    text = re_sub(r"{}{}[)dD]+|[)dD]+{}{}".format(eyes, nose, nose, eyes), " <smile> ")
    text = re_sub(r"{}{}p+".format(eyes, nose), " <lolface> ")
    text = re_sub(r"{}{}\(+|\)+{}{}".format(eyes, nose, nose, eyes), " <sadface> ")
    text = re_sub(r"{}{}[\/|l*]".format(eyes, nose), " <neutralface> ")
    text = re_sub(r"/"," / ")
    text = re_sub(r"<3"," <heart> ")
    text = re_sub(r"[-+]?[.\d]*[\d]+[:,.\d]*", "<number>")
    text = re_sub(r"#\S+", hashtag)
    text = re_sub(r"([!?.]){2,}", r"\1 <repeat> ")
    text = re_sub(r"\b(\S*?)(.)\2{2,}\b", r"\1\2 <elong> ")
    text = re_sub(r"([A-Z]){2,}", allcaps)

    text = demojize2(text)    

    text = re_sub(r"(?=[!\"#$%&\(\)*+,-./:;?@\[\]^_`{|}~])", r" ")
    text = re_sub(r"(?<=[!\"#$%&\(\)*+,-./:;?@\[\]^_`{|}~])", r" ")
    
    return text.lower()

inputs = "@bri243 This was soooo cool :) :) \nThe BEST 2 days ever!!! 💙💙💙 😊 #Epic #RoadTrip #Spain2014"  
preprocess(inputs)


# ## Loading Data

# In[4]:


datafolder = 'data/tweets/'
trainfile = 'EI-reg-En-anger-train.txt'
devfile = 'EI-reg-En-anger-dev.txt'

df_train = pd.read_csv(os.path.join(datafolder, trainfile), sep='\t')
df_dev = pd.read_csv(os.path.join(datafolder, devfile), sep='\t')


# Preprocess the tweets using the script above.

# In[5]:


df_train['Tweet'] = df_train['Tweet'].apply(preprocess)
df_dev['Tweet'] = df_dev['Tweet'].apply(preprocess)
print('Preprocessing complete.')


# ## Tokenizing Data
# We use the ``Tokenizer`` class in Keras to perform the tokenize the text. It essentially converts all text into sequences of integers. Here, the tokenizer is set to keep track of the most frequent 10,000 words. It also has a ``UNK``(unknown) token to account for words outside the dictionary. We first train the tokenizer so that it can learn the mapping between words and integers using the ``fit_on_texts()`` function.
# 
# **Note**: The present implementation in Keras is a little misleading - it doesn't correctly handle the unknown tokens well. Thus, the ad-hoc fix in the last two lines of the cell below.

# In[6]:


from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
max_tokenizer_words = 10000
tokenizer = Tokenizer(num_words=max_tokenizer_words+1, lower=True, oov_token='UNK', filters='')
tokenizer.fit_on_texts(df_train['Tweet'].append(df_dev['Tweet']))
tokenizer.word_index = {e:i for e,i in tokenizer.word_index.items() if i <= max_tokenizer_words}
tokenizer.word_index[tokenizer.oov_token] = max_tokenizer_words + 1


# We get the train and test tokens, pad the sequences. We first define a useful function ``convertToTokens()`` to perform the conversion from words to indices.

# In[7]:


max_words_in_tweet = 50
def convertToTokens(tokenizer, sentences):
    return pad_sequences(tokenizer.texts_to_sequences(sentences), 
                            maxlen=max_words_in_tweet, 
                            padding='post', truncating='post')


# In[8]:


X_train = convertToTokens(tokenizer, df_train['Tweet'])
X_dev = convertToTokens(tokenizer, df_dev['Tweet'])
y_train = df_train['Intensity Score']
y_dev = df_dev['Intensity Score']


# ## Loading Word Embeddings
# Then, load the GloVe vectors. You must set the variable ``GLOVEPATH`` appropriately. The file used in this code can be downloaded [here](http://nlp.stanford.edu/data/glove.6B.zip).

# In[23]:


import csv, os
from pathlib import Path

GLOVEPATH = os.path.join(
    'data/glove/glove.twitter.27B/glove.twitter.27B.200d.txt')
glove_index = pd.read_table(
    GLOVEPATH, sep=" ", index_col=0, header=None, quoting=csv.QUOTE_NONE)
print('Word embeddings loaded.')


# Then, create an embedding matrix to initialize the Embedding layer in the neural network. The indexing for this layer will be the same as in the tokenizer. We must initialize the rows of the embedding matrix with the word vectors of the words corresponding to the tokenizer index. These word vectors are obtained from the file at ``GLOVEPATH``. If a word is missing in ``GLOVEPATH``, its vector is initialized randomly with distribution consistent with the rest of the ``GLOVEPATH`` vectors. Also note that the number of vectors accommodated by the embedding matrix is ``max_embedding_rows=10000``, which means that the vectors of only the first 10000 words in the tokenizer index are stored.

# In[10]:


glove_mean,glove_std = np.mean(glove_index.mean()), np.mean(glove_index.std())
glove_size = len(glove_index.columns)
tokenizer_index = tokenizer.word_index

embedding_matrix = np.random.normal(glove_mean, glove_std, (max_tokenizer_words+2, glove_size))
for word, i in tokenizer_index.items():
    if word in glove_index.index: embedding_matrix[i,:] = glove_index.loc[word].values
print('Word embedding matrix created.')


# # Baseline and Traditional Models

# As a baseline, we consider linear and tree-based models on some simple lexical features. These features are extracted from the lexicons in the ``AffectiveTweets`` package in Weka. The code to extract the features is in ``ftrgen.py``, which we call here. 

# In[22]:


from ftrgen import LexiconFeatures

lexftr = LexiconFeatures(lexicondir=os.path.join('data/lexicons/'))
lexiconsPath = 'temp.csv'
lexftr.build(path=lexiconsPath)
lexftr.load(path=lexiconsPath)
X_train_lex = lexftr.vectorizeDataset(df_train['Tweet'])
X_dev_lex = lexftr.vectorizeDataset(df_dev['Tweet'])
X_train_lex = X_train_lex[[c for c in X_train_lex.columns if c != 'tweet']].values
X_dev_lex = X_dev_lex[[c for c in X_dev_lex.columns if c != 'tweet']].values


# Then, we train an SVM on them.

# In[12]:


from sklearn.svm import SVR
from scipy.stats import pearsonr
# from xgboost import XGBRegressor

model_baseline = SVR(C=0.1)
# model_baseline = XGBRegressor()

model_baseline.fit(X_train_lex, y_train)
print("Baseline Dev correlation:", pearsonr(model_baseline.predict(X_dev_lex), y_dev))
print('Baseline trained.')


# # Deep Neural Network

# We train a network comprising of a Bidirectional LSTM followed by two dense layers using Keras.

# ## Architecture Creation

# In[13]:


import keras.backend as K
from keras.layers import Input, Dense, Embedding, Dropout, Activation, Flatten, Conv1D, MaxPooling1D
from keras.layers import LSTM, Bidirectional
from keras.models import Model, Sequential
from keras import initializers, regularizers, optimizers, layers, constraints
from keras.utils import np_utils
from keras.utils import multi_gpu_model

def pearson(y_true, y_pred):
    fsp = y_pred - K.mean(y_pred) #being K.mean a scalar here, it will be automatically subtracted from all elements in y_pred
    fst = y_true - K.mean(y_true)

    devP = K.std(y_pred)
    devT = K.std(y_true)
    
    return K.mean(fsp*fst)/(devP*devT)

def simpleLSTM():
    inp = Input(shape=(max_words_in_tweet,), name='input')
    x = Embedding(max_tokenizer_words+2, glove_size, weights=[embedding_matrix], name='embedding')(inp)
    x = Bidirectional(LSTM(128, return_sequences=True, dropout=0.1, recurrent_dropout=0.1), name='bilstm')(x)
    x = Flatten()(x)
    x = Dense(100, activation='relu', name='dense1')(x)
    x = Dense(50, activation='relu', name='dense2')(x)
    x = Dense(1, activation='sigmoid', name='dense2')(x)
    model = Model(input=inp, output=x)
#     model = Model(outputs=x, inputs=inp)
    model.compile(loss='mean_squared_error', optimizer='adam', metrics=[pearson])
    return model

def deepLSTM1():
    # Stack 2 LSTMs
    inp = Input(shape=(max_words_in_tweet,), name='input')
    x = Embedding(max_tokenizer_words+2, glove_size, weights=[embedding_matrix], name='embedding')(inp)
    x = Bidirectional(LSTM(200, return_sequences=True, dropout=0.1, recurrent_dropout=0.1), name='bilstm1')(x)
    x = Bidirectional(LSTM(100, return_sequences=True, dropout=0.1, recurrent_dropout=0.1), name='bilstm2')(x)
    x = Flatten()(x)
    x = Dense(100, activation='relu', name='dense1')(x)
    x = Dense(50, activation='relu', name='dense2')(x)
    x = Dense(1, activation='sigmoid', name='dense3')(x)
    model = Model(input=inp, output=x)
#     model = Model(outputs=x, inputs=inp)
#     model.compile(loss='mean_squared_error', optimizer='adam', metrics=[pearson])

    parallel_model = multi_gpu_model(model, gpus=4)
    parallel_model.compile(loss='mean_squared_error', optimizer='adam', metrics=[pearson])

    return parallel_model

def convLSTM1():
    # Stack CNN and LSTM
    inp = Input(shape=(max_words_in_tweet,), name='input')
    x = Embedding(max_tokenizer_words+2, glove_size, weights=[embedding_matrix], name='embedding')(inp)
    x = Dropout(0.2)(x)
    x = Conv1D(filters=64, kernel_size=5, padding='valid', activation='relu', strides=1, name='conv1')(x)
    x = MaxPooling1D(pool_size=4, name='maxpool1')(x)
    x = Bidirectional(LSTM(100, return_sequences=True, dropout=0.1, recurrent_dropout=0.1), name='bilstm2')(x)
    x = Flatten()(x)
    x = Dense(100, activation='relu', name='dense1')(x)
    x = Dense(50, activation='relu', name='dense2')(x)
    x = Dense(1, activation='sigmoid', name='dense3')(x)
    model = Model(input=inp, output=x)
#     model = Model(outputs=x, inputs=inp)
    model.compile(loss='mean_squared_error', optimizer='adam', metrics=[pearson])
    return model


# ## Training and Evaluation

# We incorporate early stopping to prevent overfitting.

# In[14]:


from keras.callbacks import EarlyStopping

earlystop = EarlyStopping(monitor='val_pearson', min_delta=0.001, patience=10, verbose=1, mode='auto')
callbacks_list = [earlystop]


# Let's train the model and evaluate it on the development set.

# In[15]:


network_func = deepLSTM1
model = network_func()
modelPath = 'models/' + network_func.__name__ + '.h5'


# In[16]:


model.fit(X_train, y_train, batch_size=32, epochs=10, validation_split=0.1, callbacks=callbacks_list)
print('Correlation on development set:', model.evaluate(X_dev, y_dev, batch_size=32)[1])
if not os.path.exists('models/'): os.makedirs('models/')
model.save_weights(modelPath)


# ## Testing

# If you have pretrained weights, load them.

# In[17]:


model.load_weights(modelPath, by_name=True)


# Let's make predictions on some real inputs. First, we create a function to transform a sentence into tokens, which are required by the neural network.

# In[18]:


pd.set_option('display.max_colwidth', -1)
tokenizer_reverse_index = dict(map(reversed, tokenizer.word_index.items()))


# We make predictions on any arbitrary sentence as follows.

# In[19]:


sentences = ['i am angry and furious', 
             'i am happy',
             'the sun rises in the east',
             "This is horrible",
             "This is HORRIBLE"
            ]
labels = [None for i in sentences]
sentences = pd.Series(sentences).apply(preprocess)
df_test = pd.DataFrame({'Input': sentences, 
                        'Predictions': model.predict(convertToTokens(tokenizer, sentences)).flatten(),
#                         'Sequence': list(map(lambda x: '   '.join([str(y) for y in x]), convertToTokens(tokenizer, sentences))),            
#                         'ProcessedText': list(map(lambda x: ' '.join([tokenizer_reverse_index[y] if y in tokenizer_reverse_index else '' for y in x]), convertToTokens(tokenizer, sentences))),            
             })
df_test
# display(sentences)


# Let's look at predictions on the development set, for which we do have the labels.

# In[25]:


sentences, labels = df_dev.iloc[:20]['Tweet'], y_dev[:20]
# sentences, labels df_test_train.iloc[:50]['Tweet'], y_train[:50]

sentences = pd.Series(sentences).apply(preprocess)
df_test = pd.DataFrame({'Input': sentences, 
              'True Value': labels,
              'Predictions': model.predict(convertToTokens(tokenizer, sentences)).flatten()
             })
if labels[0]: df_test['Error'] = (df_test['True Value'] - df_test['Predictions']).apply(np.abs)
df_test.sort_values('Error', ascending=False)


# # Notes (in progress)

# While training, surprisingly, the validation correlation is much higher than the test correlation. With the basic LSTM and minimal preprocessing, the validation score was 0.65, but the test score was 0.44. Two reasons for this are: 1. overfitting, 2. discrepancy in training and test distributions in data. I improved this score to 0.66 using the following improvements:
# * 

# ## Preprocessing

# ## Data

# ## Model Selection

# ## Training

# 
# * The test distribution is drastically different from the train distribution
# * Too many epochs. * Update: Added an early stopping callback to solve this* 
# * The model must be made more expressive through the addition of more layers. *Update: Added an extra dense layer. dense2=100, dense3=25. No improvement*
# * Change the number of words in the tweets. *Update: Having 20 (corr=0.55) works better than 50 (corr=0.52) with 300d embeddings.*
# * The embeddings are not powerful enough - use larger embeddings. *Update: Using 100d embeddings, the performance increases to 0.47 on dev set. No significant different with 200d embeddings. But a jump to **0.55** with 300d embeddings*
# * Use twitter specific word embeddings. *Update: The official GloVe website has twitter specific embeddings. Training using merely 100d embeddings gives 0.54! Using 200d embeddings, we get 0.55.*
# * Observed that there are a lot of escape sequences in the text, such as \n and \t. *Removing them improved the dev performance further to 0.58! However, removing ' **reduces** the performance for some reason.*
# * Deep LSTM: 200, 100, dense(1): 0.629
# * Deep LSTM with dense(100), dense(50) in between: 0.664524867363
# * CNN-LSTM(100) with dense(50): 0.640424723478
# * CNN-LSTM(100) with dense(100) and dense(50): 0.66239
# * A **really** wide and deep network: [BalanceNet](https://github.com/tlkh/text-emotion-classification/blob/master/BalanceNet-1.0.ipynb)
# * Running note: pip install --user on nano cluster. Don't use virtual environment.
# 
